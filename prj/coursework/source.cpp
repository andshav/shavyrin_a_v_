#include <opencv2/opencv.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>

using namespace std;
using namespace cv;




struct TransformParam
{

	TransformParam(double _dx, double _dy, double _da)
	{
		dx = _dx;
		dy = _dy;
		da = _da;
	}

	double dx;
	double dy;
	double da;

	void getTransform(Mat& T)
	{
		T.at<double>(0, 0) = cos(da);
		T.at<double>(0, 1) = -sin(da);
		T.at<double>(1, 0) = sin(da);
		T.at<double>(1, 1) = cos(da);

		T.at<double>(0, 2) = dx;
		T.at<double>(1, 2) = dy;
	}
};

struct Trajectory
{
	
	Trajectory(double _x, double _y, double _a) {
		x = _x;
		y = _y;
		a = _a;
	}

	double x;
	double y;
	double a;
};


vector<Trajectory> cumsum(vector<TransformParam>& transforms)
{
	vector <Trajectory> trajectory;
	double a = 0;
	double x = 0;
	double y = 0;

	for (size_t i = 0; i < transforms.size(); i++)
	{
		x += transforms[i].dx;
		y += transforms[i].dy;
		a += transforms[i].da;

		trajectory.push_back(Trajectory(x, y, a));

	}

	return trajectory;
}

vector <Trajectory> smooth(vector <Trajectory>& trajectory, int radius)
{
	vector <Trajectory> smoothed_trajectory;
	for (size_t i = 0; i < trajectory.size(); i++) {
		double sum_x = 0;
		double sum_y = 0;
		double sum_a = 0;
		int count = 0;

		for (int j = -radius; j <= radius; j++) {
			if (i + j >= 0 && i + j < trajectory.size()) {
				sum_x += trajectory[i + j].x;
				sum_y += trajectory[i + j].y;
				sum_a += trajectory[i + j].a;

				count++;
			}
		}

		double avg_a = sum_a / count;
		double avg_x = sum_x / count;
		double avg_y = sum_y / count;

		smoothed_trajectory.push_back(Trajectory(avg_x, avg_y, avg_a));
	}

	return smoothed_trajectory;
}

float av_speed(vector<TransformParam> z)
{
	int N = z.size();
	float speed = 0;
	for (int i = 1; i < z.size(); i += 1)
	{
		speed += std::sqrt(std::pow(z[i].dx - z[i - 1].dx, 2) + std::pow(z[i].dy - z[i - 1].dy, 2));
	}
	return speed/(N-1);
}

int main(int argc, char** argv)
{
	//���������� �������� �����
	VideoCapture cap("4.avi");

	//��������� ���������� ������
	int n_frames = int(cap.get(CAP_PROP_FRAME_COUNT));

	//�������� ������ � ������ �����������
	int w = int(cap.get(CAP_PROP_FRAME_WIDTH));
	int h = int(cap.get(CAP_PROP_FRAME_HEIGHT));

	//�������� ���������� ������ � ������� (fps)(fps)
	double fps = cap.get(CAP_PROP_FPS);

	//��������� ��������� ����������
	VideoWriter out("out4.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), fps, Size(2 * w, h));

	Mat curr, curr_gray;
	Mat prev, prev_gray;

	// ������ ������� �����
	cap >> prev;

	// �������������� ����� � ������� ������
	cvtColor(prev, prev_gray, COLOR_BGR2GRAY);


	vector <TransformParam> transforms;
	Mat last_T;

	for (int i = 1; i < n_frames - 1; i++)
	{

		vector <Point2f> prev_pts, curr_pts;

		// �������� ������ �����
		goodFeaturesToTrack(prev_gray, prev_pts, 0, 0.005, 50);

		// ������ ���������� �����
		bool success = cap.read(curr);
		if (!success) break;

		cvtColor(curr, curr_gray, COLOR_BGR2GRAY);
		vector <uchar> status;
		vector <float> err;

		// ���������� ����������� ������
		calcOpticalFlowPyrLK(prev_gray, curr_gray, prev_pts, curr_pts, status, err);

		//���������� �������� �����
		auto prev_it = prev_pts.begin();
		auto curr_it = curr_pts.begin();
		for (size_t k = 0; k < status.size(); k++)
		{
			if (status[k])
			{
				prev_it++;
				curr_it++;
			}
			else
			{
				prev_it = prev_pts.erase(prev_it);
				curr_it = curr_pts.erase(curr_it);
			}
		}


		//����� ������� ��������������
		Mat T = estimateAffine2D(prev_pts, curr_pts);

		if (T.data == NULL) last_T.copyTo(T);
		T.copyTo(last_T);

		double dx = T.at<double>(0, 2);
		double dy = T.at<double>(1, 2);


		double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0));

		transforms.push_back(TransformParam(dx, dy, da));

		curr_gray.copyTo(prev_gray);
	}

	// ���������� ���������� � �������������� ������������ ����� ��������������
	vector <Trajectory> trajectory = cumsum(transforms);



	// ��������� ������� ���������� � �������������� ������� ���������� �������
	vector <Trajectory> smoothed_trajectory = smooth(trajectory, 20);


	vector <TransformParam> transforms_smooth;

	for (size_t i = 0; i < transforms.size(); i++)
	{
		// ���������� ������� ����� ���������� � �������� ���������� ��������
		double diff_x = smoothed_trajectory[i].x - trajectory[i].x;
		double diff_y = smoothed_trajectory[i].y - trajectory[i].y;
		double diff_a = smoothed_trajectory[i].a - trajectory[i].a;

		// ���������� ����� ������� ��������������
		double dx = transforms[i].dx + diff_x;
		double dy = transforms[i].dy + diff_y;
		double da = transforms[i].da + diff_a;

		transforms_smooth.push_back(TransformParam(dx, dy, da));
	}
	float av1 = av_speed(transforms);
	float av2 = av_speed(transforms_smooth);
	cout << "Input" << " " << av1 << endl;
	cout << "Stab" << " " << av2 << endl;
	cap.set(CAP_PROP_POS_FRAMES, 0);
	Mat T(2, 3, CV_64F);
	Mat frame, frame_stabilized, frame_out;


	for (int i = 0; i < n_frames - 2; i++)
	{
		cout << i << endl;
		bool success = cap.read(frame);
		if (!success) break;

		transforms_smooth[i].getTransform(T);

		// ���������� �������� ��������������
		warpAffine(frame, frame_stabilized, T, frame.size());

		hconcat(frame, frame_stabilized, frame_out);
		out.write(frame_out);

	}


	cap.release();
	out.release();

	destroyAllWindows();

	return 0;
}