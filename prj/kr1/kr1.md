# Контрольная 1.

Группа: БПМ-16-1
Студент: Шавырин А.В.

url: [Ссылка на репозиторий](http://gitlab.com/andshav/shavyrin_a_v_/)

## Задание

Подобрать небольшое изображение в градациях серого.

1. Сгенерировать серое тестовое изображение из квадратов и кругов с разными уровнями яркости (0, 127 и 255) так, чтобы присутствовали все сочетания.
2. Применить первый линейный фильтр и сделать визуализацию результата.
3. Применить второй линейный фильтр и сделать визуализацию результата.
4. Вычислить R и сделать визуализацию R.

## Результаты

![](mainImg.jpg)                                                       Рис.1.Сгенерированное изображение

------

![](mainImg1.jpg)									Рис.2. Результат применения первого линейного фильтра.

------

![](mainImg2.jpg)									Рис.3. Результат применения второго линейного фильтра.

------

![](mainImg12.jpg)                                                                       Рис.4. Визуализация R.

## Листинг кода

```cpp
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main() {

	Mat kernel1 = Mat::ones(3, 3, CV_8SC1);
	kernel1.at<int8_t>(1, 0) = 2;
	kernel1.at<int8_t>(0, 1) = 0;
	kernel1.at<int8_t>(1, 1) = 0;
	kernel1.at<int8_t>(2, 1) = 0;
	kernel1.at<int8_t>(0, 2) = -1;
	kernel1.at<int8_t>(1, 2) = -2;
	kernel1.at<int8_t>(2, 2) = -1;

	Mat kernel2;
	transpose(kernel1, kernel2);

	int size = 200;
	int radius = 50;
	Mat mainImg(size*2, size*3, CV_8UC1);
	Rect rcRoi1 = Rect(0, 0, size, size);
	Rect rcRoi2 = Rect(size, 0, size, size);
	Rect rcRoi3 = Rect(size*2, 0, size, size);
	Rect rcRoi4 = Rect(0, size, size, size);
	Rect rcRoi5 = Rect(size, size, size, size);
	Rect rcRoi6 = Rect(size*2, size, size, size);

	Mat img1(size, size, CV_8UC1, Scalar(255));
	Mat img2(size, size, CV_8UC1, Scalar(127));
	Mat img3(size, size, CV_8UC1, Scalar(0));
	Mat img4(size, size, CV_8UC1, Scalar(127));
	Mat img5(size, size, CV_8UC1, Scalar(0));
	Mat img6(size, size, CV_8UC1, Scalar(255));
	
	
	circle(img1, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(0), FILLED);
	circle(img2, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(255), FILLED);
	circle(img3, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(127), FILLED);
	circle(img4, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(0), FILLED);
	circle(img5, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(255), FILLED);
	circle(img6, Point(img1.cols / 2, img1.rows / 2), radius, Scalar(127), FILLED);

	img1.copyTo(mainImg(rcRoi1));
	img2.copyTo(mainImg(rcRoi2));
	img3.copyTo(mainImg(rcRoi3));
	img4.copyTo(mainImg(rcRoi4));
	img5.copyTo(mainImg(rcRoi5));
	img6.copyTo(mainImg(rcRoi6));
	imshow("mainImg", mainImg);
	imwrite("mainImg.jpg", mainImg);
	Mat mainImg1, mainImg2, mainImg12;
	filter2D(mainImg, mainImg1, CV_32F, kernel1);
	filter2D(mainImg, mainImg2, CV_32F, kernel2);
	imshow("mainImg1", mainImg1);
	imwrite("mainImg1.jpg", mainImg1);
	imshow("mainImg2", mainImg2);
	imwrite("mainImg2.jpg", mainImg2);
	pow(mainImg1, 2, mainImg1);
	pow(mainImg2, 2, mainImg2);
	pow((mainImg1 + mainImg2), 0.5, mainImg12);

	imshow("mainImg12", mainImg12);
	imwrite("mainImg12.jpg", mainImg12);


	waitKey(0);
	return 0;
	
}
```