# Лабораторная 4.

Группа: БПМ-16-1
Студент: Шавырин А.В.

url: [Ссылка на репозиторий](http://gitlab.com/andshav/shavyrin_a_v_/)

## Задание

Подобрать небольшое изображение в градациях серого.

1. Построить и нарисовать гистограмму распределения яркости пикселей исходного изображения.
2. Сгенерировать табличную функцию преобразования яркости. Построить график табличной функции преобразования яркости.
3. Применить табличную функцию преобразования яркости к исходному изображению и нарисовать гистограмму преобразованного изображения.
4. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения и их гистограммы).
5. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию).
6. Реализовать метод локальной бинаризации.
7. Улучшить одну из бинаризацией путем применения морфологических фильтров.
8. Сделать визуализацию бинарной маски после морфологических фильтров поверх исходного изображения.



## Результаты

![](img1.jpg)Рис. 1. Исходное изображение

------

![](imgHist1.jpg)Рис. 2. Гистограмма распределения яркостей.

------

![](graph.jpg)Рис. 3. График функции преобразования яркости.

------

![](img2.jpg)Рис. 4.  Изображение после применения функции преобразования.

------

![](imgHist2.jpg)Рис. 5. Гистограмма преобразованного изображения.

------

![](CLAHE_1.jpg)Рис. 6. Преобразование CLAHE с параметрами 6, (5, 5)

------

![](CLAHE_1_hist.jpg) Рис. 7. Гистограмма Рисунка 6.

------

![](CLAHE_2.jpg)Рис. 8. Преобразование CLAHE с параметрами 1, (10, 10)

------

![](CLAHE_2_hist.jpg)Рис. 9. Гистограмма Рисунка 8.

------

![](CLAHE_3.jpg)Рис. 10. Преобразование CLAHE с параметрами 2, (15, 15)

------

![](CLAHE_3_hist.jpg)Рис. 11. Гистограмма Рисунка 10.

------

![](global.jpg)																Рис. 12. Глобальный метод бинаризации(вычисленный порог = 138), метод Оцу

------

![](local.jpg)																Рис. 13. Метод локальной бинаризации (метод Брэдли)

------

![](local_filter.jpg)                                            				  Рис. 14. Улучшенная локальная бинаризация.

------

![](alpha.jpg)																Рис. 15. Визуализация бинарной маски

## Листинг кода

```cpp
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat hist(Mat img) {
	int hist[256] = { 0 };
	for (int i = 0; i < img.cols; i++)
	{
		for (int j = 0; j < img.rows; j++)
		{
			hist[img.at<uchar>(i, j)]++;
		}
	}
	int max = 0;
	for (int i = 0; i < 256; i++)
	{
		if (hist[i] > max) max = hist[i];
	}
	Mat imgHist = Mat::zeros(max, img.rows, IMREAD_GRAYSCALE);
	for (int i = max - 1; i >= 0; i--)
	{
		for (int j = 0; j < img.rows; j++)
		{
			if (max - 1 - i > hist[j]) imgHist.at<uchar>(i, j) = 255;
		}
	}
	resize(imgHist, imgHist, Size(256, 400), 0, 0);
	return imgHist;
}
int func(int x) {
	return abs(abs(2 * x + 100) - abs(4 * x - 300));
}
int otsuThreshold(Mat image)
{
	int min = image.at<uint8_t>(0, 0);
	int max = image.at<uint8_t>(0, 0);

	for (int i = 1; i < image.rows; i++)
	{
		for (int j = 0; j < image.cols; j++) {
			int value = image.at<uint8_t>(i, j);
			if (value < min)
				min = value;
			if (value > max)
				max = value;
		}
	}

	int hist_size = max - min + 1;
	int* hist = new int[hist_size];

	for (int t = 0; t < hist_size; t++)
		hist[t] = 0;

	for (int i = 0; i < image.rows; i++)
		for (int j = 0; j < image.cols; j++) {
			hist[image.at<uint8_t>(i, j) - min]++;
		}

	int m = 0;
	int n = 0;
	for (int t = 0; t <= max - min; t++)
	{
		m += t * hist[t];
		n += hist[t];
	}

	float max_sigma = -1;
	int threshold = 0;

	int alpha_1 = 0;
	int beta_1 = 0;

	for (int t = 0; t < max - min; t++)
	{
		alpha_1 += t * hist[t];
		beta_1 += hist[t];
		float w_1 = (float)beta_1 / n;
		float a = (float)alpha_1 / beta_1 - (float)(m - alpha_1) / (n - beta_1);
		float sigma = w_1 * (1 - w_1) * a * a;
		if (sigma > max_sigma)
		{
			max_sigma = sigma;
			threshold = t;
		}
	}

	threshold += min;
	return threshold;
}
void bradleyThreshold(Mat& inputMat, Mat& outputMat, float T, int part)
{
	int x_1, y_1, x_2, y_2, count, sum, i_p, i_t, S, s_2;
	Mat sum_Mat;
	int* p_y1, * p_y2;
	uchar* p_inputMat, * p_outputMat;

	integral(inputMat, sum_Mat);

	S = MAX(inputMat.rows, inputMat.cols);
	S = (S > (part + part)) ? (S / part) : 2;

	s_2 = S / 2;

	for (int i = 0; i < inputMat.rows; ++i)
	{
		y_1 = (i < s_2) ? 0 : (i - s_2);
		y_2 = ((i + s_2) < inputMat.rows) ? (i + s_2) : (inputMat.rows - 1);

		p_y1 = sum_Mat.ptr<int>(y_1);
		p_y2 = sum_Mat.ptr<int>(y_2);
		p_inputMat = inputMat.ptr<uchar>(i);
		p_outputMat = outputMat.ptr<uchar>(i);

		for (int j = 0; j < inputMat.cols; ++j)
		{
			x_1 = (j < s_2) ? 0 : (j - s_2);
			x_2 = ((j + s_2) < inputMat.cols) ? (j + s_2) : (inputMat.cols - 1);

			count = (x_2 - x_1) * (y_2 - y_1);

			sum = p_y2[x_2] - p_y1[x_2] - p_y2[x_1] + p_y1[x_1];

			i_p = (int)(p_inputMat[j] * count);
			i_t = (int)(sum * (1.0f - T));
			p_outputMat[j] = (i_p < i_t) ? 0 : 255;
		}
	}
}


int main() {
	Mat img = imread("../testdata/dog.jpg", IMREAD_GRAYSCALE);
	Mat imgHist = hist(img);
	resize(imgHist, imgHist, Size(256, 400), 0, 0);
	imshow("imgHist1", imgHist);
	imwrite("imgHist1.jpg", imgHist);
	imshow("img1", img);
	imwrite("img1.jpg", img);
	Mat img1 = imread("../testdata/dog.jpg", IMREAD_GRAYSCALE);
	for (int i = 0; i < img.cols; i++)
	{
		for (int j = 0; j < img.rows; j++)
		{
			img1.at<uchar>(i, j) = func(img.at<uchar>(i, j));
		}
	}
	imgHist = hist(img1);

	imshow("imgHist2", imgHist);
	imwrite("imgHist2.jpg", imgHist);
	imshow("img2", img1);
	imwrite("img2.jpg", img1);
	Mat graph(256, 256, CV_8UC1, Scalar(255));
	for (int i = 0; i < 256; i++)
	{
		graph.at<uchar>(255-func(i), i) = 0;
	}


	imshow("graph", graph);
	imwrite("graph.jpg", graph);


	Mat clahe_img;
	Ptr<CLAHE> clahe = createCLAHE();
	clahe->setClipLimit(6);
	clahe->setTilesGridSize(Size(5, 5));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_1 = hist(clahe_img);
	imshow("Hist CLAHE 1", clahe_hist_1);
	imwrite("CLAHE_1_hist.jpg", clahe_hist_1);
	imshow("CLAHE 1", clahe_img);
	imwrite("CLAHE_1.jpg", clahe_img);

	clahe->setClipLimit(1);
	clahe->setTilesGridSize(Size(10, 10));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_2 = hist(clahe_img);
	imshow("Hist CLAHE 2", clahe_hist_2);
	imwrite("CLAHE_2_hist.jpg", clahe_hist_2);
	imshow("CLAHE 2", clahe_img);
	imwrite("CLAHE_2.jpg", clahe_img);

	clahe->setClipLimit(2);
	clahe->setTilesGridSize(Size(15, 15));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_3 = hist(clahe_img);
	imshow("Hist CLAHE 3", clahe_hist_3);
	imwrite("CLAHE_3_hist.jpg", clahe_hist_3);
	imshow("CLAHE 3", clahe_img);
	imwrite("CLAHE_3.jpg", clahe_img);

	int threshold = otsuThreshold(img);
	cout << threshold;
	Mat global(img.rows, img.cols, CV_8UC1);
	Mat main_img = Mat::zeros(img.rows, img.cols * 2, CV_8UC1);
	for (int i = 0; i < img.rows; i += 1)
	{
		for (int j = 0; j < img.cols; j += 1)
		{
			if (img.at<uchar>(i, j) > threshold)
				global.at<uchar>(i, j) = 255;
			else
				global.at<uchar>(i, j) = 0;
		}
	}
	img.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	global.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Global binarization", main_img);
	imwrite("global.jpg", main_img);

	float T = 0.1;
	int part = 8;
	Mat local = Mat::zeros(img.size(), CV_8UC1);
	bradleyThreshold(img, local, T, part);
	img.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	local.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Local binarization", main_img);
	imwrite("local.jpg", main_img);

	Mat morph;
	morphologyEx(local, morph, MORPH_OPEN, Mat::ones(2, 2, CV_32F));
	local.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	morph.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Morphology local for global binarization", main_img);
	imwrite("local_filter.jpg", main_img);

	Mat alpha;
	addWeighted(img, 0.5, morph, 0.5, 1.0, alpha);
	morph.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	alpha.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Alpha blending", main_img);
	imwrite("alpha.jpg", main_img);


	waitKey(0);
	return 0;
}


```